package com.example.testworkexample.viewModel

import androidx.lifecycle.ViewModel
import com.example.testworkexample.model.repository.ItemRepository

@Deprecated("deprecatedCardView")
class ItemViewModel : ViewModel() {

     val itemRepository = ItemRepository.newInstance()

    fun getItemsFromServer() = itemRepository.getItemsFromServer()
}
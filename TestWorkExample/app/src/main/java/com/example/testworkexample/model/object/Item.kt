package com.example.testworkexample.model.`object`

import com.example.testworkexample.ui.adapter.Orientation
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
data class Item(
    @SerializedName("id")
    @Expose
    var id: Long,
    @SerializedName("name")
    @Expose
    var name: String,
    @SerializedName("price")
    @Expose
    var price: Long,
    @SerializedName("created_at")
    @Expose
    var dateOfCreation: Long,
    @SerializedName("image")
    @Expose
    var image: Image?,
    var viewType: Orientation = Orientation.LIST
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Image(
    @SerializedName("id")
    @Expose
    var id: Long,
    @SerializedName("thumb")
    @Expose
    var thumb: String,
    @SerializedName("original")
    @Expose
    var original: String
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class ListItems (
    @SerializedName("count")
    @Expose
    var count: Long,
    @SerializedName("data")
    @Expose
    var data: List<Item>
)
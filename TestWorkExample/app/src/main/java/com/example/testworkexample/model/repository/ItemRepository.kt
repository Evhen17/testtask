package com.example.testworkexample.model.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.testworkexample.model.`object`.Item
import com.example.testworkexample.model.`object`.ListItems
import com.example.testworkexample.model.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Deprecated("deprecated")
class ItemRepository{

    var items: MutableLiveData<List<Item>> = MutableLiveData()
    val rclient = RetrofitClient.create()

    companion object{
        fun newInstance() = ItemRepository()
    }

    fun getItemsFromServer() :  LiveData<List<Item>>  {
         val call: Call<ListItems> = rclient.getAllData()
        call.enqueue(object : Callback<ListItems>{
            override fun onResponse(call: Call<ListItems>, response: Response<ListItems>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        items.postValue(it.data)
                    }
                }
            }
            override fun onFailure(call: Call<ListItems>, t: Throwable) {
                t.printStackTrace()
            }
        })
        return items
    }
}

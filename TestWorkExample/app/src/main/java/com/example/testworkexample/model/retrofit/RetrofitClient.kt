package com.example.testworkexample.model.retrofit

import com.example.testworkexample.model.`object`.ListItems
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.POST


interface RetrofitClient {

    @POST("filter")
    fun getAllData(): Call<ListItems>

    companion object RetrofitGetInstance {
        val baseUrl = "https://kupisever.ru/api/advertisement/"
        fun create(): RetrofitClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .build()

            return retrofit.create(RetrofitClient::class.java)
        }
    }
}
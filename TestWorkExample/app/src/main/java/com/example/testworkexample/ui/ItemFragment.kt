package com.example.testworkexample.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testworkexample.R
import com.example.testworkexample.model.`object`.Item
import com.example.testworkexample.ui.adapter.ItemPaggingAdapter
import com.example.testworkexample.ui.adapter.Orientation
import com.example.testworkexample.viewModel.DataSourceViewModel
import kotlinx.android.synthetic.main.fragment_item.*


class ItemFragment : Fragment() {

    val adapter = ItemPaggingAdapter()
    private lateinit var viewModel: DataSourceViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onListener()
        initRecyclerViewWithLinearLayoutManager()
        initObserers()
    }

    private fun onListener() {
        sort.setOnClickListener { view?.findNavController()?.navigate(R.id.sortingFragment) }
        switch_cards_imageView.setOnClickListener { toggleCardViewType() }
        switch_list_imageView.setOnClickListener { toggleCardViewType() }
    }

    fun initObserers(){
        viewModel = ViewModelProviders.of(this).get(DataSourceViewModel::class.java)
        viewModel.itemPagedList?.observe(this,
            Observer<PagedList<Item>> { items -> adapter.submitList(items) })
    }

    private fun initRecyclerViewWithLinearLayoutManager(){
        recycler_view.layoutManager = LinearLayoutManager(requireContext())
        recycler_view.hasFixedSize()
        recycler_view.adapter = adapter
    }

    private fun toggleCardViewType() {
            if (adapter.getItems()?.get(0)?.viewType == Orientation.LIST) {
                recycler_view.layoutManager = GridLayoutManager(requireContext(), 2)
                adapter.getItems()?.forEach { it.viewType = Orientation.CARD }
                recycler_view.adapter = adapter
                switch_cards_imageView.visibility = View.GONE
                switch_list_imageView.visibility = View.VISIBLE

            } else {
                recycler_view.layoutManager = LinearLayoutManager(requireContext())
                adapter.getItems()?.forEach { it.viewType = Orientation.LIST }
                recycler_view.adapter = adapter
                switch_cards_imageView.visibility = View.VISIBLE
                switch_list_imageView.visibility = View.GONE
            }
    }
}

package com.example.testworkexample.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import com.example.testworkexample.model.`object`.Item
import com.example.testworkexample.model.dataSource.DataSourceFactory

class DataSourceViewModel : ViewModel() {

    var itemPagedList: LiveData<PagedList<Item>>?=null
    var liveDataSource: LiveData<PageKeyedDataSource<Int, Item>>

    init {

        val itemDataSourceFactory = DataSourceFactory()

        liveDataSource = itemDataSourceFactory.getItemLiveData()

        val config: PagedList.Config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(1)
            .build()

        itemPagedList = LivePagedListBuilder(itemDataSourceFactory, config).build()
    }
}
package com.example.testworkexample.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testworkexample.R
import com.example.testworkexample.model.`object`.Item
import kotlinx.android.synthetic.main.item_switch_card.view.*
import kotlinx.android.synthetic.main.item_switch_list.view.*
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat

class ItemPaggingAdapter : PagedListAdapter<Item, RecyclerView.ViewHolder>(ItemDiffUtilCallback()) {

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position)?.viewType?.ordinal == Orientation.CARD.ordinal) {
            Orientation.CARD.ordinal
        } else
            Orientation.LIST.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_switch_list, parent, false)
        var viewHolder: RecyclerView.ViewHolder = ListItemViewHolder(view)

        if (viewType == Orientation.CARD.ordinal) {
            viewHolder = CardItemViewHolder(layoutInflater.inflate(R.layout.item_switch_card, parent, false))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CardItemViewHolder) {
            holder.bindCard(getItem(position))
        }
        else if (holder is ListItemViewHolder) {
            holder.bindList(getItem(position))
        }
    }

    fun getItems() = currentList

    class ItemDiffUtilCallback : DiffUtil.ItemCallback<Item>() {
        override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem == newItem
        }
    }

    class ListItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var dateFormat: SimpleDateFormat
        fun bindList(item: Item?) {

            Glide.with(itemView.context)
                .load(
                    item?.image?.let { it.thumb }
                        ?: "https://kupisever.ru/image/post/264/thumb_15727809665dbebba605e7c.jpg"
                )
                .into(itemView.item_imageView)

            dateFormat = SimpleDateFormat("dd MMMM HH:mm", myDateFormatSymbols)

            itemView.name.text = item?.name
            itemView.price.text = item?.price.toString()
            itemView.data_of_creation.text = dateFormat.format(item?.dateOfCreation)
        }
    }

    class CardItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var dateFormat: SimpleDateFormat
        fun bindCard(item: Item?) {

            Glide.with(itemView.context)
                .load(
                    item?.image?.let { it.thumb }
                        ?: "https://kupisever.ru/image/post/264/thumb_15727809665dbebba605e7c.jpg"
                )
                .into(itemView.card_imageView)

            dateFormat = SimpleDateFormat("dd MMMM HH:mm", myDateFormatSymbols)

            itemView.card_name.text = item?.name
            itemView.card_price.text = item?.price.toString()
            itemView.card_data_of_creation.text = dateFormat.format(item?.dateOfCreation)
        }
    }

    companion object {
        private val myDateFormatSymbols = object : DateFormatSymbols() {
            override fun getMonths(): Array<String> {
                return arrayOf(
                    "января",
                    "февраля",
                    "марта",
                    "апреля",
                    "мая",
                    "июня",
                    "июля",
                    "августа",
                    "сентября",
                    "октября",
                    "ноября",
                    "декабря"
                )
            }
        }
    }
}
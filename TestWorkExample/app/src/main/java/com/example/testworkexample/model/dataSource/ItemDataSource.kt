package com.example.testworkexample.model.dataSource


import androidx.paging.PageKeyedDataSource
import com.example.testworkexample.model.`object`.Item
import com.example.testworkexample.model.`object`.ListItems
import com.example.testworkexample.model.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ItemDataSource : PageKeyedDataSource<Int, Item>() {

    val FIRST_PAGE = 1

    override fun loadInitial(
        params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Item>
    ) {
        RetrofitClient.create().getAllData()
            .enqueue(object : Callback<ListItems> {
                override fun onResponse(call: Call<ListItems>, response: Response<ListItems>) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            response.body()?.data?.let { it1 ->
                                callback.onResult(
                                    it1,
                                    null,
                                    FIRST_PAGE + 1
                                )
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ListItems>, t: Throwable) {
                    t.printStackTrace()
                }
            })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {
        RetrofitClient.create().getAllData(
        )
            .enqueue(object : Callback<ListItems> {

                override fun onResponse(call: Call<ListItems>, response: Response<ListItems>) {
                    if (response.body() != null) {
                        val key = if (params.key > 1) {
                            params.key
                        } else {
                            null
                        }
                        response.body()?.data?.let { callback.onResult(it, key) }
                    }
                }

                override fun onFailure(call: Call<ListItems>, t: Throwable) {
                    t.printStackTrace()
                }
            })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {
        RetrofitClient.create().getAllData(
        )
            .enqueue(object : Callback<ListItems> {

                override fun onResponse(call: Call<ListItems>, response: Response<ListItems>) {
                    if (response.body() != null) {
                        val key = if (params.key < 1) {
                            params.key
                        } else null
                        response.body()?.data?.let { callback.onResult(it, key) }
                    }
                }

                override fun onFailure(call: Call<ListItems>, t: Throwable) {
                    t.printStackTrace()
                }
            })
    }
}
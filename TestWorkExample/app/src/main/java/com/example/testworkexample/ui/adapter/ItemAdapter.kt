package com.example.testworkexample.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testworkexample.R
import com.example.testworkexample.model.`object`.Item
import kotlinx.android.synthetic.main.item_switch_list.view.*

@Deprecated("deprecatedCardView")
class ItemAdapter : RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {

    private var list = mutableListOf<Item>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ItemViewHolder(
            layoutInflater.inflate(
                R.layout.item_switch_list,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = list[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: Item) {
            Glide.with(itemView.context)
                .load(
                    item.image?.thumb ?:"https://kupisever.ru/image/post/264/thumb_15727809665dbebba605e7c.jpg")
                .into(itemView.item_imageView)


            itemView.name.text = item.name
            itemView.price.text = item.price.toString()
            itemView.data_of_creation.text = item.dateOfCreation.toString()
        }
    }
}
package com.example.testworkexample.model.dataSource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.example.testworkexample.model.`object`.Item

class DataSourceFactory : DataSource.Factory<Int, Item>(){

    private val liveData: MutableLiveData<PageKeyedDataSource<Int, Item>> = MutableLiveData()

    override fun create(): DataSource<Int, Item> {
        val itemDataSource = ItemDataSource()
        liveData.postValue(itemDataSource)
        return itemDataSource
    }

    fun getItemLiveData(): MutableLiveData<PageKeyedDataSource<Int, Item>>{
        return liveData
    }
}
package com.example.testworkexample.ui.adapter

enum class Orientation { CARD, LIST }